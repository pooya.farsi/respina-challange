import React, {useState} from 'react'
import Sidebar from "../components/Sidebar";
import BookList from "../components/BookList";


const Books = () => {
  const [SelectedCats, setSelectedCats] = useState([])

  return (
    <>
      <Sidebar SelectedCats={SelectedCats} setSelectedCats={setSelectedCats}/>
      <BookList SelectedCats={SelectedCats}/>
    </>
  )
}

export default Books