import React from 'react'
import user from './../mockData/user.json'
import {LOGIN_ERROR} from "../constant/Login";

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            error: ''
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        e.preventDefault()
        let {name , value} = e.target
        if(name === 'email') {
            this.setState({email: value})
        }
        else if (name === 'password') {
            this.setState({password: value})
        }
        this.setState({error: ''})
    }

    handleSubmit(e) {
        e.preventDefault()
        let {email, password} = this.state
        if (email === user.email && password === user.password) {
            localStorage.setItem('authorization', 'xxx')
            this.props.history.push('./')
        }
        else
            this.setState({error: LOGIN_ERROR})
    }



    render() {
        let {email , password, error} = this.state
        return (
            <div className="login-wrapper">
                <div className="login-form">
                    <form onSubmit={(e) => this.handleSubmit(e)}>
                        <input type="text" placeholder="آدرس ایمیل" name="email" value={email} onChange={(e) => this.handleChange(e)} />
                        <input type="password" placeholder="کلمه عبور" name="password" value={password} onChange={(e) => this.handleChange(e)} />
                        <button type="submit">ورود</button>
                        {error && <p>{error}</p>}
                    </form>
                </div>
            </div>
        )
    }
}

export default Login
