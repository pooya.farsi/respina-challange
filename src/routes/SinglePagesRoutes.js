import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Books from "../pages/Books";

const SinglePagesRoutes = () => (
    <Switch>
        <Route path="/" exact={true} component={Books} />
    </Switch>
)

export default SinglePagesRoutes