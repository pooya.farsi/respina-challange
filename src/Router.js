import React from 'react'
import {Route, Switch, BrowserRouter} from 'react-router-dom'
import Login from "./auth/Login";
import PagesLayout from "./components/PagesLayout";
import NotFound from "./components/NotFound";

const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact={true} component={PagesLayout}/>
                <Route path="/login" exact={true} component={Login}/>
                <Route path="/*" component={NotFound} />
            </Switch>
        </BrowserRouter>
    )
}
export default Router