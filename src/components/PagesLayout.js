import React, { Component } from 'react';
import Header from "./Header";
import SinglePagesRoutes from "../routes/SinglePagesRoutes";
import Footer from "./Footer";

class PagesLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {
        return (
            <content className="wrapper">
                <Header/>
                <div className="main-content">
                    <SinglePagesRoutes/>
                </div>
                <Footer/>
            </content>
        );
    }
}

export default PagesLayout;