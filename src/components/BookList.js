import React, {useState, useEffect} from 'react'
import books from './../mockData/books.json'
import {filter as _filter, isEmpty as _isEmpty} from 'lodash'
import Pagination from "./Pagination";
import {PAGINATION_PAGE_COUNT} from "../constant/Pagination";
import {Modal} from "./Modal";

const BookList = ({SelectedCats}) => {
    const [Items, SetItems] = useState([])
    const [CurrentPage, setCurrentPage] = useState(1)

    useEffect(() => {
        let items = _isEmpty(SelectedCats) ?
            books
            :
            _filter(books, book => SelectedCats.indexOf(book.category_id) !== -1)
        SetItems(items)
    }, [SelectedCats])


    const renderBooks = () => {
        let itemsToShow = calcPagination()

        return itemsToShow.length ?
            itemsToShow.map((item) => (
                <div className="book"
                     key={item.id}
                     onClick={() => Modal(item.image,item.title,item.summary,item.translator,item.author_id)}>
                    <img src={item.image} alt={item.title}/>
                    <h6>{item.title}</h6>
                    <p>{item.translator}</p>
                </div>
            ))
            :
            <h3>مطلبی در این دسته بندی یافت نشد.</h3>
    }

    const calcPagination = () => {
        return Items.slice(PAGINATION_PAGE_COUNT * (CurrentPage - 1), PAGINATION_PAGE_COUNT * (CurrentPage - 1) + PAGINATION_PAGE_COUNT)
    }

    return (
        <div className="content-wrapper">
            <div className="book-wrapper">
                {renderBooks()}
            </div>
            <Pagination setCurrentPage={setCurrentPage} pageCount={Items.length / PAGINATION_PAGE_COUNT}/>
        </div>
    )
}

export default BookList