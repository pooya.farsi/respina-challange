import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import Logo from './../assets/logo.png'
import Banner from './../assets/banner.png'
import user from './../mockData/user.json'

const Header = () => {

    const [CheckAuth, setCheckAuth] = useState(false)
    const [UserData, setUserData] = useState({})

    useEffect(() => {
        // Get User Data from API if User has Token
        let authorization = localStorage.getItem('authorization')
        if(authorization) {
            setCheckAuth(true)
            getUserInfo()
        }

    },[])

    const getUserInfo = () => {
        // GET DATA FROM DUMMY DATA
        setUserData(user)
    }

    const handleLogout = () => {
        localStorage.removeItem('authorization')
        setCheckAuth(false)
    }


    return (
        <div className="header">
            <div className="top-bar">
                <div className="right-col">
                    <img src={Logo} alt="بنر کتاب"/>
                </div>

                <div className="left-col">
                    {
                        CheckAuth ?
                            <span>{UserData.full_name} |
                                <span className="logout" onClick={()=> handleLogout()}>خروج</span>
                            </span>
                            :
                            <Link className="login" to='/login'>ورود</Link>
                    }
                </div>
            </div>
            <img src={Banner} alt="Header Banner" className="header-banner"/>
        </div>
    )
}

export default Header
