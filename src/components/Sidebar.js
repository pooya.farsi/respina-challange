import React from 'react'
import categories from './../mockData/categories.json'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCheckSquare, faSquare} from '@fortawesome/free-regular-svg-icons'
import {find as _find, filter as _filter} from 'lodash'


const Sidebar = ({SelectedCats, setSelectedCats}) => {

  const handleSelectedCats = (id) => {
    let exist = _find(SelectedCats, (cat) => (cat === id))
    if (exist) {
      setSelectedCats(_filter(SelectedCats, cat => cat !== id))
    } else {
      setSelectedCats(currentCats => [...currentCats, id]);
    }
  }

  const renderCategories = () => {
    return categories.map((category) => (
      <span key={category.id} onClick={() => handleSelectedCats(category.id)}>
                <FontAwesomeIcon
                  icon={_find(SelectedCats, (cat) => (cat === category.id)) ? faCheckSquare : faSquare}/>
        {category.title}
            </span>
    ))
  }

  return (
    <div className="sidebar">
      <h4 className="sidebar-title">دسته بندی ها</h4>
      <div className="categories">
        {renderCategories()}
      </div>
    </div>
  )
}

export default Sidebar
