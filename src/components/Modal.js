import React from 'react';
import {PopupboxManager} from 'react-popupbox';

export const Modal = (image,title,summary,translator,author_id) => {
    let content = (
        <div className="modal-wrapper">
            <img src={image} alt={title} />
            <div className="modal-content">
                <h3>{title}</h3>
                <p>نویسنده: {author_id}</p>
                <p>مترجم: {translator}</p>
                <p>{summary}</p>
            </div>
        </div>
    );
    PopupboxManager.open({
        content,
        config: {
            titleBar: {
                enable: true,
                text: title
            },
        },
    });
};