import React from 'react'

const Footer = () => {

    return (
        <div className="footer">
            <div className="cols">
                <div className="footer-col">
                    <h6>آخرین مطالب</h6>
                    <ul>
                        <li>ردیف اول</li>
                        <li>ردیف دوم</li>
                        <li>ردیف سوم</li>
                        <li>ردیف چهارم</li>
                    </ul>
                </div>
                <div className="footer-col">
                    <h6>دسترسی سریع</h6>
                    <ul>
                        <li>ردیف اول</li>
                        <li>ردیف دوم</li>
                        <li>ردیف سوم</li>
                        <li>ردیف چهارم</li>
                    </ul>
                </div>
                <div className="footer-col">
                    <h6>آخرین پروژه ها</h6>
                    <ul>
                        <li>ردیف اول</li>
                        <li>ردیف دوم</li>
                        <li>ردیف سوم</li>
                        <li>ردیف چهارم</li>
                    </ul>
                </div>
                <div className="footer-col">
                    <h6>درباره ما</h6>
                    <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است.
                    </p>
                </div>
            </div>
            <div className="copyright">
                تمامی حقوق محفوظ است.
            </div>
        </div>
    )
}

export default Footer
