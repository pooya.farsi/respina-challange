import React from 'react'
import Logo from './../assets/logo.png'


const NotFound = () => {

    return (
        <div className="">
            <img src={Logo} alt="لوگو"/>
            <p>صفحه مورد نظر یافت نشد</p>
        </div>
    )
}

export default NotFound
