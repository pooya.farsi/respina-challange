import React from 'react'
import ReactPaginate from 'react-paginate';


class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handlePageClick = this.handlePageClick.bind(this);
  }

  handlePageClick = (data) => {
    this.props.setCurrentPage(data.selected + 1)
  }

  render () {
    let {pageCount} = this.props

    return (
      <ReactPaginate
        previousLabel={'قبلی'}
        nextLabel={'بعدی'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={this.handlePageClick}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        activeClassName={'active'}
      />
    )
  }
}

export default Pagination